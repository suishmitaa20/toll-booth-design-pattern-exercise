/**
 * @author Suishmitaa Srianand
 *
 */
package edu.pnw.tollbooth;

/**
 * Maintains truck information such as the number of axles and the total weight of the truck.
 */
public interface Truck {
  /**
   * Gets the number of axles in the truck
   * 
   * @return the number of axles
   */
  public int getAxles();

  /**
   * Gets the total weight of the truck
   * 
   * @return the total weight
   */
  public float getWeight();
}
