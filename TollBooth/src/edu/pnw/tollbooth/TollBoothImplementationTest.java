/**
 * @author Suishmitaa Srianand
 */
package edu.pnw.tollbooth;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 * Unit test cases for the operations performed by the toll booth.
 */
class TollBoothImplementationTest {

  /**
   * Unit test case to test if the toll amount is calculated correctly.
   */
  @Test
  void calculateTollTest() {
    TollBooth booth = new AlleghenyTollBooth();

    Truck ford = new FordTruck(5, 12000); // Normal input
    booth.calculateToll(ford);
    assertEquals(145, ((AlleghenyTollBooth) booth).getTollAmount());

    Truck nissan = new NissanTruck(0, 12000); // No axle
    booth.calculateToll(nissan);
    assertEquals(120, ((AlleghenyTollBooth) booth).getTollAmount());

    Truck daewoo = new DaewooTruck(5, 0); // No weight
    booth.calculateToll(daewoo);
    assertEquals(25, ((AlleghenyTollBooth) booth).getTollAmount());

    Truck ford1 = new FordTruck(0, 0); // No axle no weight
    booth.calculateToll(ford1);
    assertEquals(0, ((AlleghenyTollBooth) booth).getTollAmount());
  }

  /**
   * Unit test case to test if the toll booth maintains the total number of trucks passed since last
   * collection correctly
   */
  @Test
  void checkTotalTrucksTest() {
    TollBooth booth = new AlleghenyTollBooth();
    Truck ford = new FordTruck(5, 12000);

    booth.calculateToll(ford); // One truck arrives
    assertEquals(1, ((AlleghenyTollBooth) booth).getTotalTrucks());

    booth.calculateToll(ford); // Two trucks since last collection
    assertEquals(2, ((AlleghenyTollBooth) booth).getTotalTrucks());

    booth.collectReceipt(); // Collecting receipts and resetting value
    assertEquals(0, ((AlleghenyTollBooth) booth).getTotalTrucks());
  }

  /**
   * Unit test case to test if the toll booth maintains the total receipts since last collection
   * correctly
   */
  @Test
  void checkTotalReceiptsTest() {
    TollBooth booth = new AlleghenyTollBooth();
    Truck ford = new FordTruck(5, 12000);

    booth.calculateToll(ford); // One receipt processed
    assertEquals(145, ((AlleghenyTollBooth) booth).getTotalReceipts());

    booth.calculateToll(ford); // Two receipts since last collection
    assertEquals(290, ((AlleghenyTollBooth) booth).getTotalReceipts());

    booth.collectReceipt(); // Collecting receipts and resetting value
    assertEquals(0, ((AlleghenyTollBooth) booth).getTotalReceipts());
  }
}
