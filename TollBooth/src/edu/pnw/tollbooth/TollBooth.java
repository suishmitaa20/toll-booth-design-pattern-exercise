package edu.pnw.tollbooth;

/**
 * Interface which manages the operations performed by a toll booth. It maintains the cost
 * chargeable per axle and per weight of the incoming vehicle.
 */
public interface TollBooth {
  
  /**
   * Calculates the toll based on the axle and weight data provided by the incoming vehicle.
   * 
   * @param make Truck object for which the toll is to be calculated
   */
  public void calculateToll(Truck make);

  /**
   * Displays the total number of trucks that passed through the toll booth and the totals receipts
   * collected since last collection.
   */
  public void displayData();

  /**
   * Displays the totals and then resets them to zero.
   */
  public void collectReceipt();
}
