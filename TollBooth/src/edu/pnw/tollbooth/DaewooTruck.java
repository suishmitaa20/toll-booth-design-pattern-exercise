/**
 * @author Suishmitaa Srianand
 */
package edu.pnw.tollbooth;

/**
 * Implements the Truck interface. It has the truck information such as the number of axles and the
 * weight of the truck.
 */
class DaewooTruck implements Truck {

  private int axles;
  private float weight;

  public DaewooTruck(int axles, float weight) {
    this.axles = axles;
    this.weight = weight;
  }

  /*
   * (non-Javadoc)
   * 
   * @see edu.pnw.tollbooth.Truck#getAxles()
   */
  @Override
  public int getAxles() {
    return axles;
  }

  /*
   * (non-Javadoc)
   * 
   * @see edu.pnw.tollbooth.Truck#getWeight()
   */
  @Override
  public float getWeight() {
    return weight;
  }

}
