/**
 * @author Suishmitaa Srianand
 * @version java 1.8
 */


package edu.pnw.tollbooth;

/**
 * This class simulates the operations of a toll booth.
 */
public class TollBoothImplementation {

  /**
   * Main method.
   * @param args Array of String elements.
   */
  public static void main(String[] args) {
    TollBooth booth = new AlleghenyTollBooth();
    Truck ford = new FordTruck(5, 12000); // 5 axles and 12000 kilograms
    Truck nissan = new NissanTruck(2, 5000); // 2 axles and 5000kg
    Truck daewoo = new DaewooTruck(6, 17000); // 6 axles and 17000kg
    booth.calculateToll(ford);
    booth.calculateToll(nissan);
    booth.displayData();
    booth.collectReceipt();
    booth.calculateToll(daewoo);
    booth.displayData();
  }
}
