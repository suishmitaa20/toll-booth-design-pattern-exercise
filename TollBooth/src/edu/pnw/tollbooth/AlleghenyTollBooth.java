/**
 * @author Suishmitaa Srianand
 *
 */
package edu.pnw.tollbooth;

/**
 * A class that implements the TollBooth interface. The toll is calculated here. In addition to
 * that,the booth maintains the total number of trucks and the receipts collected since the last
 * collection.
 */
class AlleghenyTollBooth implements TollBooth {

  private int totalTrucks = 0;
  private float totalReceipts = 0;
  private float tollAmount;
  private float axleCost = 5;
  private float weightCost = 10;

  public AlleghenyTollBooth() {
    this.tollAmount = 0;
  }

  public float getTollAmount() {
    return tollAmount;
  }

  public int getTotalTrucks() {
    return totalTrucks;
  }

  public float getTotalReceipts() {
    return totalReceipts;
  }

  /*
   * (non-Javadoc)
   * 
   * @see edu.pnw.tollbooth.TollBooth#calculateToll(edu.pnw.tollbooth.Truck)
   */
  @Override
  public void calculateToll(Truck make) {
    int axles = make.getAxles();
    float weight = make.getWeight();
    tollAmount = (axles * axleCost) + ((weight / 1000) * weightCost);
    totalReceipts += tollAmount;
    totalTrucks++;
    System.out.println("Truck arrival - Axles: " + axles + " Total weight: " + weight
        + " Toll due: $" + tollAmount);
  }

  /*
   * (non-Javadoc)
   * 
   * @see edu.pnw.tollbooth.TollBooth#displayData()
   */
  @Override
  public void displayData() {
    System.out.println(
        "Totals since last collection - Receipts: $" + totalReceipts + "  Trucks: " + totalTrucks);
  }

  /*
   * (non-Javadoc)
   * 
   * @see edu.pnw.tollbooth.TollBooth#collectReceipt()
   */
  @Override
  public void collectReceipt() {
    System.out.println("*** Collecting receipts  ***");
    displayData();
    totalTrucks = 0;
    totalReceipts = 0;
  }

}
